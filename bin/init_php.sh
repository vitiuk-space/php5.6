#!/bin/bash
set -e

cp -R /app/etc/* /etc

chmod 750 /etc/service/*/run

mkdir -p /run/php

# make terminal programs happy, eg. vim, less
echo "export TERM=xterm-256color" >> /root/.bashrc

# PHP-FPM

# Backup original information
cp /etc/php/5.6/fpm/php.ini /etc/php/5.6/fpm/php.ini.dist
cp /etc/php/5.6/fpm/pool.d/www.conf /etc/php/5.6/fpm/pool.d/www.conf.dist

# Still necessary in case of misconfiguration in sites-enabled/
sed -i -r "s/;cgi.fix_pathinfo\s*=\s*1/cgi.fix_pathinfo=0/g" /etc/php/5.6/fpm/php.ini

# Don't fork
sed -i -r "s/;daemonize = yes/daemonize = no/g" /etc/php/5.6/fpm/php-fpm.conf

sed -i -r "s/short_open_tag = Off/short_open_tag = On/g" /etc/php/5.6/fpm/php.ini

# Catch PHP output
sed -i -r "s/;catch_workers_output =/catch_workers_output =/g" /etc/php/5.6/fpm/pool.d/www.conf

# Fix socket permissions
sed -i -r "s/;listen.mode = 0660/listen.mode = 0750/g" /etc/php/5.6/fpm/pool.d/www.conf

#Secure Cookies
#sed -i -r "s/;session.cookie_secure =/session.cookie_secure = True/g" /etc/php/5.6/fpm/php.ini

# Enable sendmail additional parameters
sed -i -r "s/;sendmail_path =/sendmail_path =/g" /etc/php/5.6/fpm/php.ini

# Clear upstream data
rm /app/www/index.html
echo "<?php phpinfo(); " > /app/www/index.php
